package com.cpe.groupe3.transactionmicroservicepublic.dto;

import java.io.Serializable;

public class PostTransactionDTO implements Serializable {
	private static final long serialVersionUID = 194220491356140067L;
	private Integer id;
	private Integer userId;
	private Integer cardInstanceId;
	
	public PostTransactionDTO() {
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getCardInstanceId() {
		return cardInstanceId;
	}

	public void setCardInstanceId(int cardInstanceId) {
		this.cardInstanceId = cardInstanceId;
	}
}
