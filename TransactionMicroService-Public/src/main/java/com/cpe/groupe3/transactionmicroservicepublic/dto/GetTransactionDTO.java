package com.cpe.groupe3.transactionmicroservicepublic.dto;

public class GetTransactionDTO {
	private Integer id;
	private Integer userId;
	private Integer cardInstanceId;
	private TransactionAction action;
	
	public GetTransactionDTO() {
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getCardInstanceId() {
		return cardInstanceId;
	}

	public void setCardInstanceId(int cardInstanceId) {
		this.cardInstanceId = cardInstanceId;
	}

	public TransactionAction getAction() {
		return action;
	}

	public void setAction(TransactionAction action) {
		this.action = action;
	}
}
