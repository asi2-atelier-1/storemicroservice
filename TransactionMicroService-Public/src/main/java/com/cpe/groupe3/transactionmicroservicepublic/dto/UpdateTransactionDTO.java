package com.cpe.groupe3.transactionmicroservicepublic.dto;

import java.io.Serializable;

public class UpdateTransactionDTO implements Serializable {
	private static final long serialVersionUID = -6900623949540280573L;
	private Integer id;
    private Boolean isCardOk;
	private Boolean isUserOk;
	
	public UpdateTransactionDTO() {
	}
	
	public UpdateTransactionDTO(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	public Boolean isCardOk() {
		return isCardOk;
	}
	
	public void setCardOk(Boolean isCardOk) {
		this.isCardOk = isCardOk;
	}
	
	public Boolean isUserOk() {
		return isUserOk;
	}
	
	public void setUserOk(Boolean isUserOk) {
		this.isUserOk = isUserOk;
	}	
}
