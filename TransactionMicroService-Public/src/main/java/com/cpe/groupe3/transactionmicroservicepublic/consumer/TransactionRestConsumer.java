package com.cpe.groupe3.transactionmicroservicepublic.consumer;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.cpe.groupe3.commonmicroservice.consumer.RestConsumer;
import com.cpe.groupe3.transactionmicroservicepublic.dto.GetTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.PostTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.UpdateTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.rest.ITransactionRest;




@Component("transactionRestConsumer")
public class TransactionRestConsumer extends RestConsumer implements ITransactionRest {

    public TransactionRestConsumer() {
        super("8082");
    }

    @Override
    public List<GetTransactionDTO> getAllTransactions() {
        return Arrays.asList(getForEntity(ALL, GetTransactionDTO[].class));
    }
    
    @Override
    public List<GetTransactionDTO> getTransactionsForUser(Integer userId){
        return Arrays.asList(getForEntity(FOR_USER, GetTransactionDTO[].class, userId));
    }
    
    @Override
    public List<GetTransactionDTO> getTransactionsInPendingForUser(Integer userId) {
        return Arrays.asList(getForEntity(FOR_USER_IN_PENDING, GetTransactionDTO[].class, userId));
    }
    
    @Override
    public void buyCard(PostTransactionDTO order) {
        postForEntity(BUY, order, void.class);
    }

    @Override
    public void sellCard(PostTransactionDTO order) {
        postForEntity(SELL, order, void.class);
    }

    @Override
	public void updateTransaction(UpdateTransactionDTO tD, Integer id) {
        patchForEntity(UPDATE, tD, UpdateTransactionDTO.class, id);
	}
}
