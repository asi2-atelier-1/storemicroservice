package com.cpe.groupe3.transactionmicroservicepublic.rest;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cpe.groupe3.transactionmicroservicepublic.dto.GetTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.PostTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.UpdateTransactionDTO;

public interface ITransactionRest {
    public final String ROOT_PATH = "/transactions";

    public final String ALL = ROOT_PATH + "";
    public final String UPDATE = ROOT_PATH + "/update/{id}";
    public final String FOR_USER = ROOT_PATH + "/user/{userId}";
    public final String FOR_USER_IN_PENDING = ROOT_PATH + "/pending/user/{userId}";
    public final String BUY = ROOT_PATH + "/buy";
    public final String SELL = ROOT_PATH + "/sell";

    @RequestMapping(method = RequestMethod.GET, value = ALL)
	public List<GetTransactionDTO> getAllTransactions();
    
    @RequestMapping(method = RequestMethod.GET, value = FOR_USER)
	public List<GetTransactionDTO> getTransactionsForUser(Integer userId);

    @RequestMapping(method = RequestMethod.GET, value = FOR_USER_IN_PENDING)
	public List<GetTransactionDTO> getTransactionsInPendingForUser(Integer userId);
    
    @RequestMapping(method = RequestMethod.POST, value = BUY)
	public void buyCard(PostTransactionDTO order);

    @RequestMapping(method = RequestMethod.POST, value = SELL)
	public void sellCard(PostTransactionDTO order);

    @RequestMapping(method = RequestMethod.PATCH, value = UPDATE)
    public void updateTransaction(UpdateTransactionDTO tD, Integer id);
}
