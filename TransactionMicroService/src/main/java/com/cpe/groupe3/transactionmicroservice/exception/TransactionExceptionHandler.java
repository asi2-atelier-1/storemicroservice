package com.cpe.groupe3.transactionmicroservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.cpe.groupe3.commonmicroservice.dto.ErrorDTO;

@RestControllerAdvice
public class TransactionExceptionHandler {
    @ExceptionHandler(TransactionNotFoundException.class)
    public ResponseEntity<ErrorDTO> handleTransactionNotFoundException(final TransactionNotFoundException ex){
        return new ResponseEntity<ErrorDTO>(new ErrorDTO("Invalid Transaction", ex.getMessage(), HttpStatus.NOT_FOUND.value(), TransactionNotFoundException.class.getSimpleName()), HttpStatus.NOT_FOUND);
    }
}
