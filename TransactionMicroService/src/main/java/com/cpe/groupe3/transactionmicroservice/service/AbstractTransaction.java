package com.cpe.groupe3.transactionmicroservice.service;

import com.cpe.groupe3.cardmicroservicepublic.consumer.CardInstanceRestConsumer;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceDTO;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceTransactionDTO;
import com.cpe.groupe3.commonmicroservice.dto.NotificationAction;
import com.cpe.groupe3.commonmicroservice.exception.NotificationException;
import com.cpe.groupe3.transactionmicroservice.model.TransactionModel;
import com.cpe.groupe3.transactionmicroservice.repository.TransactionRepository;
import com.cpe.groupe3.transactionmicroservicepublic.dto.PostTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.TransactionAction;
import com.cpe.groupe3.usermicroservicepublic.consumer.UserRestConsumer;
import com.cpe.groupe3.usermicroservicepublic.dto.UserDTO;
import com.cpe.groupe3.usermicroservicepublic.dto.UserTransactionDTO;

abstract class AbstractTransaction {
    public abstract String checkTransaction(CardInstanceDTO cID, UserDTO uD);
    public abstract TransactionAction getTransactionAction();
    public abstract UserDTO updateUser(UserDTO uD, float cardPrice);
    public abstract CardInstanceDTO updateCardInstance(CardInstanceDTO cID, Integer userId);

    public final void transactionCard(PostTransactionDTO order, UserRestConsumer uRC, CardInstanceRestConsumer cIRC, TransactionRepository tR) throws NotificationException {
        UserDTO uD = uRC.get(order.getUserId().toString());
        CardInstanceDTO cID = cIRC.getCardInstance(order.getCardInstanceId());

        String reasonFailure = checkTransaction(cID, uD);

        if (reasonFailure == ""){
            TransactionModel sT = new TransactionModel(order.getUserId(), order.getCardInstanceId(), getTransactionAction());
            TransactionModel res = tR.save(sT);
            order.setId(res.getId());

            uD = updateUser(uD, cID.getPrice());
            cID  = updateCardInstance(cID, uD.getId());

            CardInstanceTransactionDTO cIT = new CardInstanceTransactionDTO(cID, order.getId());
            UserTransactionDTO userTD = new UserTransactionDTO(uD, order.getId());

            // Should I check something ?
            cIRC.updateCardInstanceForTransaction(cIT, cID.getIdCardInstance());
            uRC.updateForTransaction(userTD, uD.getId());
        }else{
            throw new NotificationException(reasonFailure, uD.getId(), NotificationAction.NOTIFY);
        }
    }
}
