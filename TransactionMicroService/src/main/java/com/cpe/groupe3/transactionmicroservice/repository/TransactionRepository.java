package com.cpe.groupe3.transactionmicroservice.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.cpe.groupe3.transactionmicroservice.model.TransactionModel;

@Repository
public interface TransactionRepository extends CrudRepository<TransactionModel, Integer> {
	@Query(value = "SELECT t from TransactionModel t WHERE (t.isCardOk = FALSE OR t.isUserOk = FALSE) AND userId = ?1")
	public List<TransactionModel> findPendingByUserId(Integer userId);
	public List<TransactionModel> findByUserId(Integer userId);
	
}
