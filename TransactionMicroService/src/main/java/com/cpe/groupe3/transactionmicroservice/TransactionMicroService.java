package com.cpe.groupe3.transactionmicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;

@SpringBootApplication
@ComponentScan(basePackages = { "com.cpe.groupe3.authmicroservicepublic", "com.cpe.groupe3.cardmicroservicepublic", "com.cpe.groupe3.usermicroservicepublic", "com.cpe.groupe3.commonmicroservice" })
@ComponentScan
@OpenAPIDefinition(info = @Info(title = "Transaction MicroService Rest Api", version = "1.0", description = "Information about the Transaction MicroService APi and how to interact with"))
public class TransactionMicroService {

	public static void main(String[] args) {
		SpringApplication.run(TransactionMicroService.class, args);
	}

}

