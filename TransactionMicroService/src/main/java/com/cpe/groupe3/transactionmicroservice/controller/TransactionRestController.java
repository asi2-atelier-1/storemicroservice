package com.cpe.groupe3.transactionmicroservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.cpe.groupe3.transactionmicroservice.service.DispatcherTransactionService;
import com.cpe.groupe3.transactionmicroservicepublic.dto.GetTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.PostTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.UpdateTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.rest.ITransactionRest;

//ONLY FOR TEST NEED ALSO TO ALLOW CROOS ORIGIN ON WEB BROWSER SIDE
@CrossOrigin
@RestController
public class TransactionRestController implements ITransactionRest {
	
	@Autowired
	private DispatcherTransactionService dispatcherTransactionService;

	public TransactionRestController() {
	}

	@Override
	public List<GetTransactionDTO> getAllTransactions() {
		return dispatcherTransactionService.getAllTransactions();
	}
	
	@Override
	public List<GetTransactionDTO> getTransactionsForUser(@PathVariable Integer userId) {
		return dispatcherTransactionService.getTransactionsForUser(userId);
	}
	
	@Override
	public List<GetTransactionDTO> getTransactionsInPendingForUser(@PathVariable Integer userId) {
		return dispatcherTransactionService.getTransactionsInPendingForUser(userId);
	}

	@Override
	public void buyCard(@RequestBody PostTransactionDTO order) {
		dispatcherTransactionService.buyCard(order);	
	}

	@Override
	public void sellCard(@RequestBody PostTransactionDTO order) {
		dispatcherTransactionService.sellCard(order);
	}

	@Override
	public void updateTransaction(@RequestBody UpdateTransactionDTO tD, @PathVariable Integer id) {
		tD.setId(id);
		dispatcherTransactionService.updateTransaction(tD); 
	}
}
