package com.cpe.groupe3.transactionmicroservice.service;

import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.TransactionAction;
import com.cpe.groupe3.usermicroservicepublic.dto.UserDTO;

public class TransactionBuy extends AbstractTransaction{
    @Override
    public String checkTransaction(CardInstanceDTO cID, UserDTO uD) {
        if (uD.getAccount() < cID.getPrice()){
            return "You don't have enough money to buy this Card ("+ String.valueOf(cID.getPrice()) +")!";
        }
        if (cID.getUserId() != null){
            return "This card belongs to someone else !";
        }
        return "";
    }

    @Override
    public TransactionAction getTransactionAction() {
        return TransactionAction.BUY;
    }

    @Override
    public UserDTO updateUser(UserDTO uD, float cardPrice) {
        uD.setAccount(uD.getAccount() - cardPrice);
        return uD;
    }

    @Override
    public CardInstanceDTO updateCardInstance(CardInstanceDTO cID, Integer userId){
        cID.setUserId(userId);
        return cID;
    }
}
