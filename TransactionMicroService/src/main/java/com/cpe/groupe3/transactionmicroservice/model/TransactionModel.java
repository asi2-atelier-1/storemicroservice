package com.cpe.groupe3.transactionmicroservice.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.cpe.groupe3.transactionmicroservicepublic.dto.TransactionAction;

@Entity
public class TransactionModel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private Integer userId;
	private Integer cardInstanceId;
	private TransactionAction action;
    private java.sql.Timestamp timeSt;
    private Boolean isCardOk;
	private Boolean isUserOk;
	
	public TransactionModel() {
		this.timeSt=new Timestamp(System.currentTimeMillis());
	}

	public TransactionModel( Integer userId, Integer cardInstanceId, TransactionAction action) {
		super();
		this.userId = userId;
		this.cardInstanceId = cardInstanceId;
		this.action = action;
		this.timeSt=new Timestamp(System.currentTimeMillis());
		this.isCardOk = null;
		this.isUserOk = null;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getCardInstanceId() {
		return cardInstanceId;
	}

	public void setCardInstanceId(Integer cardInstanceId) {
		this.cardInstanceId = cardInstanceId;
	}

	public TransactionAction getAction() {
		return action;
	}

	public void setAction(TransactionAction action) {
		this.action = action;
	}

	public java.sql.Timestamp getTimeSt() {
		return timeSt;
	}

	public void setTimeSt(java.sql.Timestamp sqlTimestamp) {
		this.timeSt = sqlTimestamp;
	}
	
    public Boolean getIsCardOk() {
		return isCardOk;
	}

	public void setIsCardOk(Boolean isCardOk) {
		this.isCardOk = isCardOk;
	}

	public Boolean getIsUserOk() {
		return isUserOk;
	}

	public void setIsUserOk(Boolean isUserOk) {
		this.isUserOk = isUserOk;
	}
}
