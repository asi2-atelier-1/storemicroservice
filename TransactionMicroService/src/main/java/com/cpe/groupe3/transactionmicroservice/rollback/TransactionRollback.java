package com.cpe.groupe3.transactionmicroservice.rollback;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cpe.groupe3.cardmicroservicepublic.consumer.CardInstanceRestConsumer;
import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceDTO;
import com.cpe.groupe3.transactionmicroservice.model.TransactionModel;
import com.cpe.groupe3.transactionmicroservicepublic.dto.TransactionAction;
import com.cpe.groupe3.usermicroservicepublic.consumer.UserRestConsumer;
import com.cpe.groupe3.usermicroservicepublic.dto.UserDTO;

@Component
public class TransactionRollback {
    @Autowired
    private UserRestConsumer userRestConsumer;
    @Autowired
    private CardInstanceRestConsumer cardInstanceRestConsumer;

    public void rollBackUser(TransactionModel transaction){
        UserDTO uD = userRestConsumer.get(transaction.getId().toString());
        CardInstanceDTO cID = cardInstanceRestConsumer.getCardInstance(transaction.getCardInstanceId());
        if (transaction.getAction() == TransactionAction.BUY){
            uD.setAccount(uD.getAccount() + cID.getPrice());
        }else{
            uD.setAccount(uD.getAccount() - cID.getPrice());
        }
        userRestConsumer.update(uD, uD.getId().toString());
    }

    public void rollBackCardInstance(TransactionModel transaction){
        UserDTO uD = userRestConsumer.get(transaction.getId().toString());
        CardInstanceDTO cID = cardInstanceRestConsumer.getCardInstance(transaction.getCardInstanceId());
        if (transaction.getAction() == TransactionAction.BUY){
            cID.setUserId(null);
        }else{
            cID.setUserId(uD.getId());
        }
        cardInstanceRestConsumer.updateCardInstance(cID, cID.getIdCardInstance());
    }
}
