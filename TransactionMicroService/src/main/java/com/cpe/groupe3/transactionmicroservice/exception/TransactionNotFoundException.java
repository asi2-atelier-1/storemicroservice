package com.cpe.groupe3.transactionmicroservice.exception;

public class TransactionNotFoundException extends RuntimeException {
    public TransactionNotFoundException(String message){
        super(message);
    }
}
