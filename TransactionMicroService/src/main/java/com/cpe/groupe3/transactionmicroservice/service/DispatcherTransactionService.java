package com.cpe.groupe3.transactionmicroservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cpe.groupe3.commonmicroservice.message.QueueMessage;
import com.cpe.groupe3.transactionmicroservicepublic.dto.GetTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.PostTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.UpdateTransactionDTO;

@Component
public class DispatcherTransactionService implements ITransactionService{
	@Autowired TransactionService transactionService;

	public void buyCard(PostTransactionDTO order) {
		transactionService.post(new QueueMessage<PostTransactionDTO>(order, "buyCard"));
	}

	public void sellCard(PostTransactionDTO order) {
		transactionService.post(new QueueMessage<PostTransactionDTO>(order, "sellCard"));
	}

	public List<GetTransactionDTO> getAllTransactions() {
		return transactionService.getAllTransactions();
	}
	
	public List<GetTransactionDTO> getTransactionsForUser(Integer userId) {
		return transactionService.getTransactionsForUser(userId);
	}
	
	public List<GetTransactionDTO> getTransactionsInPendingForUser(Integer userId) {
		return transactionService.getTransactionsInPendingForUser(userId);
	}

	@Override
	public void updateTransaction(UpdateTransactionDTO uTD) {
		transactionService.post(new QueueMessage<UpdateTransactionDTO>(uTD, "updateTransaction"));
	}
}
