package com.cpe.groupe3.transactionmicroservice.mapper;

import java.sql.Timestamp;
import java.util.Date;

import com.cpe.groupe3.transactionmicroservice.model.TransactionModel;
import com.cpe.groupe3.transactionmicroservicepublic.dto.GetTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.PostTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.TransactionAction;
import com.cpe.groupe3.transactionmicroservicepublic.dto.UpdateTransactionDTO;

public class TransactionMapper {
	public static GetTransactionDTO fromTransactionModelToGetTransactionDto(TransactionModel tM) {
		GetTransactionDTO tD = new GetTransactionDTO();
		tD.setId(tM.getId());
		tD.setCardInstanceId(tM.getCardInstanceId());
		tD.setUserId(tM.getUserId());
		tD.setAction(tM.getAction());
		return tD;
	}
	
	public static TransactionModel fromPostTransactionDtoToTransactionModel(PostTransactionDTO tD, TransactionAction action) {
		TransactionModel tM = new TransactionModel();
		tM.setId(tD.getId());
		tM.setCardInstanceId(tD.getCardInstanceId());
		tM.setUserId(tD.getUserId());
		tM.setTimeSt(new Timestamp((new Date()).getTime()));
		tM.setIsCardOk(false);
		tM.setIsUserOk(false);
		tM.setAction(action);
		return tM;
	}
	
	public static TransactionModel fromUpdateTransactionDtoToModel(UpdateTransactionDTO uTD) {
		TransactionModel tM = new TransactionModel();
		tM.setIsCardOk(uTD.isCardOk());
		tM.setIsUserOk(uTD.isUserOk());
		tM.setId(uTD.getId());
		return tM;
	}
}
