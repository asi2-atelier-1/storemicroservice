package com.cpe.groupe3.transactionmicroservice.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.cpe.groupe3.commonmicroservice.dto.NotificationAction;
import com.cpe.groupe3.commonmicroservice.exception.NotificationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cpe.groupe3.cardmicroservicepublic.consumer.CardInstanceRestConsumer;
import com.cpe.groupe3.commonmicroservice.service.QueueService;
import com.cpe.groupe3.transactionmicroservice.exception.TransactionNotFoundException;
import com.cpe.groupe3.transactionmicroservice.mapper.TransactionMapper;
import com.cpe.groupe3.transactionmicroservice.model.TransactionModel;
import com.cpe.groupe3.transactionmicroservice.repository.TransactionRepository;
import com.cpe.groupe3.transactionmicroservicepublic.dto.GetTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.PostTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.UpdateTransactionDTO;
import com.cpe.groupe3.usermicroservicepublic.consumer.UserRestConsumer;

@Service
public class TransactionService extends QueueService implements ITransactionService{
	@Autowired
	private UserRestConsumer userRestConsumer;
	@Autowired
	private CardInstanceRestConsumer cardInstanceRestConsumer;
	@Autowired
	private TransactionRepository transactionRepository;
	
	public TransactionService() {
	}

	public void buyCard(PostTransactionDTO order) throws NotificationException {
		new TransactionBuy().transactionCard(order, userRestConsumer, cardInstanceRestConsumer, transactionRepository);
	}

	public void sellCard(PostTransactionDTO order) throws NotificationException {
		new TransactionSell().transactionCard(order, userRestConsumer, cardInstanceRestConsumer, transactionRepository);
	}

	public List<GetTransactionDTO> getAllTransactions() {
		List<GetTransactionDTO> sTList = new ArrayList<>();
		for (TransactionModel t : this.transactionRepository.findAll()) {
			sTList.add(TransactionMapper.fromTransactionModelToGetTransactionDto(t));
		}
		return sTList;
	}
	
	public List<GetTransactionDTO> getTransactionsForUser(Integer userId) {
		userRestConsumer.get(userId.toString()); // Checking user exists		
		List<GetTransactionDTO> sTList = new ArrayList<>();
		for (TransactionModel t : this.transactionRepository.findByUserId(userId)) {
			sTList.add(TransactionMapper.fromTransactionModelToGetTransactionDto(t));
		}
		return sTList;
	}
	
	public List<GetTransactionDTO> getTransactionsInPendingForUser(Integer userId) {
		userRestConsumer.get(userId.toString()); // Checking user exists	

		List<GetTransactionDTO> sTList = new ArrayList<>();
		for (TransactionModel t : this.transactionRepository.findPendingByUserId(userId)) {
			sTList.add(TransactionMapper.fromTransactionModelToGetTransactionDto(t));
		}
		return sTList;
	}

	@Override
	public void updateTransaction(UpdateTransactionDTO uTD) throws NotificationException {
		TransactionModel transactionToChange;
		Optional<TransactionModel> tM = transactionRepository.findById(uTD.getId());
		if (tM.isPresent()){
			transactionToChange = tM.get();
		}else{
			throw new TransactionNotFoundException("Transaction not found");
		}

		if (uTD.isUserOk() != null){
			transactionToChange.setIsUserOk(uTD.isUserOk());
		}
		else if (uTD.isCardOk() != null){
			transactionToChange.setIsCardOk(uTD.isCardOk());
		}

		try{
			transactionRepository.save(transactionToChange);
		} catch (Exception e){
			// TODO : Rollback
			throw new NotificationException("There was an error while dealing with your transaction", transactionToChange.getUserId(), NotificationAction.NOTIFY);
		}

		if (Boolean.TRUE.equals(transactionToChange.getIsCardOk()) && Boolean.TRUE.equals(transactionToChange.getIsUserOk())) {
			throw new NotificationException("Transaction successful !", transactionToChange.getUserId(), NotificationAction.UPDATE);
		}
		else if (Boolean.FALSE.equals(transactionToChange.getIsCardOk()) || Boolean.FALSE.equals(transactionToChange.getIsUserOk())) {
			// TODO : Rollback
			throw new NotificationException("There was an error while updating your account or the Card, please retry", transactionToChange.getUserId(), NotificationAction.NOTIFY);
		}
	}
}

