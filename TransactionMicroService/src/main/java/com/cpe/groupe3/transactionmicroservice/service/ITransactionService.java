package com.cpe.groupe3.transactionmicroservice.service;

import java.util.List;

import com.cpe.groupe3.commonmicroservice.exception.NotificationException;
import com.cpe.groupe3.transactionmicroservicepublic.dto.GetTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.PostTransactionDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.UpdateTransactionDTO;

public interface ITransactionService {
	public void buyCard(PostTransactionDTO order) throws NotificationException;
	public void sellCard(PostTransactionDTO order) throws NotificationException;
	public List<GetTransactionDTO> getAllTransactions();
	public List<GetTransactionDTO> getTransactionsForUser(Integer userId);
	public List<GetTransactionDTO> getTransactionsInPendingForUser(Integer userId);
	public void updateTransaction(UpdateTransactionDTO uTD) throws NotificationException;
}
