package com.cpe.groupe3.transactionmicroservice.service;

import com.cpe.groupe3.cardmicroservicepublic.dto.CardInstanceDTO;
import com.cpe.groupe3.transactionmicroservicepublic.dto.TransactionAction;
import com.cpe.groupe3.usermicroservicepublic.dto.UserDTO;

public class TransactionSell extends AbstractTransaction{
    @Override
    public String checkTransaction(CardInstanceDTO cID, UserDTO uD) {
        if (cID.getUserId() != uD.getId()){
            return "This card does not belong to you !";
        }
        return "";
    }

    @Override
    public TransactionAction getTransactionAction() {
        return TransactionAction.SELL;
    }

    @Override
    public UserDTO updateUser(UserDTO uD, float cardPrice) {
        uD.setAccount(uD.getAccount() + cardPrice);
        return uD;
    }

    @Override
    public CardInstanceDTO updateCardInstance(CardInstanceDTO cID, Integer userID) {
        cID.setUserId(null);
        return cID;
    }
}
